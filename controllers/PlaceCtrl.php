<?php

class PlaceCtrl extends Ctrl {


	public function insertAct () {
		$placeMod = $this -> getModel("Place");
		if ($_POST) {	
			$msg = $placeMod -> validate($_POST); 
			if ($msg == '') {
				$res = $placeMod -> insert($_POST['name'],$_POST['descr'],$_POST['id']);
				if (!$res) $msg = "Не могу создать полку.Ошибка БД!";
				else $msg = "ОК. Новая полка создана!";	
			} 	
			$_SESSION['msg'] = $msg;
			header("location: $_SERVER[HTTP_REFERER]");
		} 
	}


	public function deleteAct () {
		$placeMod = $this -> getModel("Place");
		if (!$_REQUEST['place_id']) {
			$_SESSION['msg'] = "Не указана полка для удаления!";
			header("location: $_SERVER[HTTP_REFERER]");
			die;
		}
		$res = $placeMod -> delete((int) $_REQUEST['place_id']);
		if ($res != "") $_SESSION['msg'] = $res;
		else $_SESSION['msg'] = "ОК. Полка удалена!";
		header("location: $_SERVER[HTTP_REFERER]");			
	}

}