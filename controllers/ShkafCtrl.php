<?php

class ShkafCtrl extends Ctrl {


	public function selectAct () {
				
		$shkafMod = $this -> getModel("Shkaf");
		$roomMod = $this -> getModel("Room");

		$rooms = $roomMod -> selectAll();
		$res = $shkafMod -> selectAll();
		$view = $this -> render('shkaf_select',compact('res','rooms'));

		return $this -> layout($view);
		
	}

	public function newAct () {
				
		$shkafMod = $this -> getModel("Shkaf");

		if ($_POST) {	
			$msg = $shkafMod -> validate($_POST); 
			if ($msg == '') {
				$res = $shkafMod -> insert($_POST['name'],$_POST['descr'],$this->loadPic('pic'),$_POST['room_id']);
				if (!$res) $msg = "Не могу создать шкаф.Ошибка БД!";
				else $msg = "ОК. Новый шкаф создан!";	
			} 	
			$_SESSION['msg'] = $msg;
			header("location: $_SERVER[HTTP_REFERER]");
		} 
	}

 	public function updateAct () {

		$shkafMod = $this -> getModel("Shkaf");

		if ($_POST) {	
			$msg = $shkafMod -> validate($_POST); 
			if ($msg == '') {
				$res = $shkafMod -> update($_POST['id'],$_POST['name'],$_POST['descr'],$this->loadPic('pic'));
				if (!$res) $msg = "Не могу изменить шкаф. Ошибка БД!";
				else $msg = "ОК. Шкаф изменен!";	
			} 	
			$_SESSION['msg'] = $msg;
			header("location: $_SERVER[HTTP_REFERER]");
		} 

	}

	public function deleteAct () {

		$shkafMod = $this -> getModel("Shkaf");

		$res = $shkafMod -> delete((int) $_REQUEST['id']);
		if ($res != "") {
			$_SESSION['msg'] = $res;
			header("location: $_SERVER[HTTP_REFERER]");			
		} else {
			header("location:?c=shkaf&a=select");
			$_SESSION['msg'] = "ОК. Шкаф удален!";			
		}		

	}
	
 	public function roomAct () {

		$shkafMod = $this -> getModel("Shkaf");

		$res = $shkafMod -> roomChange($_POST['id'],$_POST['room_id']);
		if (!$res) $msg = "Не могу переставить шкаф. Ошибка БД!";
		else $msg = "ОК. Шкаф переставлен в другую локацию!";	

		$_SESSION['msg'] = $msg;
		header("location: $_SERVER[HTTP_REFERER]");

	}

	public function detailAct () {

		$shkafMod = $this -> getModel("Shkaf");
		$thingMod = $this -> getModel("Thing");
		$placeMod = $this -> getModel("Place");
		$peopleMod = $this -> getModel("People");
		$roomMod = $this -> getModel("Room");


		$shkaf = $shkafMod -> selectOne((int)$_REQUEST["id"]);
		if ($shkaf) {
			$places = $placeMod -> selectAllInShkaf((int)$_REQUEST["id"]);
			$people = $peopleMod -> selectAll();
			$things = $thingMod->selectShkaf((int)$_REQUEST["id"]);
			$rooms = $roomMod -> selectAll();
			$view = $this->render('shkaf_detail',compact('shkaf','places','people','things','rooms'));
		} else $view = "<div class='info'>Такой информации нет в БД!</div>";
		
		return $this -> layout($view);
		
	}
}