<?php

class ThingCtrl extends Ctrl {
	
	public function selectAct () {
	
		$thingMod = $this -> getModel("Thing");
		$peopleMod = $this -> getModel("People");

		$res = -1;		
		if (!$_GET['name'] && !$_GET['descr'] && !$_GET['shkaf'] && !$_GET['people']) { }
		else {
			$search['name'] = $_GET['name'];
			$search['descr'] = $_GET['descr'];
			$search['shkaf'] = $_GET['shkaf'];
			$search['people'] = $_GET['people'];
			$res = $thingMod -> selectSearch($_GET['name'], $_GET['descr'], $_GET['shkaf'],$_GET['people']);
		}
		
		$people = $peopleMod -> selectAll();

		$view = $this -> render('thing_select',compact('res','people','search'));
		
		return $this -> layout($view);
		
	}

	public function newAct () {
		$thingMod = $this -> getModel("Thing");

		if ($_POST) {	
			$msg = $thingMod -> validate($_POST); 
			if ($msg == '') {
				$ins = $thingMod -> insert($_POST['name'],$_POST['descr'], $_POST['place_id'], $this -> loadPic('pic'),$_POST['people_id']);
				if (!$ins) $msg = "Невозможно создать вещь. Ошибка БД!";
				else $msg = "ОК. Новая вещь создана!";	
			} 		
			$_SESSION['msg'] = $msg;
			header("location: $_SERVER[HTTP_REFERER]");
		} 		
	}

	public function updateAct () {
		$thingMod = $this -> getModel("Thing");
		
		$msg = $thingMod -> validate($_POST); 
		if ($msg == '') {
			$res = $thingMod -> update($_POST['id'],$_POST['name'],$_POST['descr'],$this->loadPic('pic'));
			if (!$res) $msg = "Невозможно изменить вещь. Ошибка БД!";
			else $msg = "ОК. Вещь изменена!";	
		} 	

		$_SESSION['msg'] = $msg;
		header("location: $_SERVER[HTTP_REFERER]");
	}

	public function deleteAct () {
		$thingMod = $this -> getModel("Thing");

		$res = $thingMod -> delete((int) $_REQUEST['id']);
		if ($res != "") {
			$_SESSION['msg'] = $res;
			header("location: $_SERVER[HTTP_REFERER]");
		} else {
			$_SESSION['msg'] = "ОК. Вещь удалена!";
			header("location:?c=thing&a=select");	
		}
	}
	
	public function moveAct () {
		$thingMod = $this -> getModel("Thing");

		$res = $thingMod -> move($_POST['id'],$_POST['place_id']);
		if (!$res) $msg = "Невозможно переместить вещь. Ошибка БД!";
		else $msg = "ОК. Вещь перемещена!";	

		$_SESSION['msg'] = $msg;
		header("location: $_SERVER[HTTP_REFERER]");
	}

	public function ownerAct () {
		$thingMod = $this -> getModel("Thing");

		$res = $thingMod -> changeOwner($_POST['id'],$_POST['owner_id']);
		if (!$res) $msg = "Невозможно поменять владельца. Ошибка БД!";
		else $msg = "ОК. Владелец вещи изменен!";	

		$_SESSION['msg'] = $msg;
		header("location: $_SERVER[HTTP_REFERER]");
	}

	public function detailAct () {
		$thingMod = $this -> getModel("Thing");
		$placeMod = $this -> getModel("Place");
		$peopleMod = $this -> getModel("People");

		$thing = $thingMod -> selectOne((int)$_REQUEST["id"]);
		if ($thing) { 
			$places = $placeMod -> selectAll();
			$owners = $peopleMod -> selectAll();
			$view = $this->render('thing_detail',compact('thing','places','owners'));			
		} else $view = "<div class='info'>Такой информации нет в БД!</div>";

		return $this -> layout($view);
	}
	
}