<?php

class Ctrl {

	public function getModel($name) {		
		require_once("../models/Model.php");

		if (file_exists("../models/".$name."Mod.php")) require("../models/".$name."Mod.php"); 
		else exit("Нет файла с моделью ".$name);	

		$modClass = $name."Mod";
		if (class_exists($modClass)) $modObj = new $modClass; 
		else exit("Нет класса для модели ".$name);	

		return $modObj; 			
	}

	public function render($view,$data) {
		extract($data);
		
		ob_start();
		require("../views/".$view.".php");
		return ob_get_clean();
 		
	}
	
	public function layout($content) {			
		require("../views/main.php");
	}

	public function loadPic($pic) {
		$picfile = '';
		if(isset($_FILES) && $_FILES[$pic]['error'] == 0) { 
			$img = new Imagick($_FILES[$pic]['tmp_name']);
			$w = $img->getImageWidth();
			$h = $img->getImageHeight();
			if ($w>$h && $w>1200) $img->resizeImage(1200,0,Imagick::FILTER_CATROM,1);
			else if ($h>1200) $img -> resizeImage(0,1200,Imagick::FILTER_CATROM,1);
			$orientation = $img->getImageOrientation();
			switch($orientation) { 
				case imagick::ORIENTATION_BOTTOMRIGHT: 
					$img->rotateimage("#000", 180); // rotate 180 degrees 
				break; 
				case imagick::ORIENTATION_RIGHTTOP: 
					$img->rotateimage("#000", 90); // rotate 90 degrees CW 
				break; 
				case imagick::ORIENTATION_LEFTBOTTOM: 
					$img->rotateimage("#000", -90); // rotate 90 degrees CCW 
				break; 
			}
			$img -> stripImage();
			$img->writeImage($_FILES[$pic]['tmp_name']);
			$img->clear();
			$img->destroy();			
			$picfile = IMG_DIR.date("ymdHis").strrchr($_FILES[$pic]['name'],"."); 
			move_uploaded_file($_FILES[$pic]['tmp_name'], $picfile );

		}
		return $picfile; 
	}

	public function noPic($pic) {
		if ($pic == "") return IMG_DIR."no.jpg";
		else return $pic; 
	}
}