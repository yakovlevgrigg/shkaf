<div class='boxes p05'>
	<a class='no_ref' href='#'>
		<div class='btn' onClick='document.getElementById("new_shkaf_wnd").style.display = "flex"'> Создать новый шкаф</div>
	</a>
</div>

<form class='wnd rows sd_cnt' action='?c=shkaf&a=new' method='post'enctype="multipart/form-data" id="new_shkaf_wnd">
	<div class='rows brd p05 m05 sd_cnt sdw'>
		<div class='head p05'>Новый шкаф</div>
		<div>
			<div class='p05'>
				<span class='lbl'>Название</span><span style='color:red;'>* </span><br>
				<input class='fld full' type='text' name='name'>
			</div>
			<div class='p05'> 
				<span class='lbl'>Описание</span> <br>
				<textarea class='fld full' name='descr' ></textarea>
			</div>
			<div class='p05'>
				<span class='lbl'>Добавить фото</span> <br> 
				<input class='fld full' type="file" name='pic'>
			</div>
			<div class='p05'>
				<span class='lbl'>Локация</span> <br>
				<select class='fld full' name='room_id'>
					<?php
						foreach ($rooms as $room) {
							echo "<option value=".$room['id'].">".$room['name']."</option>";
						}
					?>
				</select> 
			</div>
		</div>	
		<div class='wait p05' id='wait'>
			...грузим в БД...
		</div>
		<div class='p05'>		
			<button class='btn m05_r' onClick='document.getElementById("wait").style.display = "block"' type='submit' >Создать</button>
			<button class='btn' onClick='document.getElementById("new_shkaf_wnd").style.display = "none"' type='button' >Нет</button>
		</div>
	</div>
</form>

<div>
	
	<?php
	$room = '000';
	if ($res) {
		echo "<div class='head m05_l'>Список всех шкафов</div>";
		foreach ($res as $line) {
			$r = $line["room_name"];
			if ($room != $r) {
				if ($room != '000') echo "</div>";
				echo "<div class='list_head shkf_hd'> 
						Локация <i>".$r."</i>
					 </div>
					 <div class='boxes wrp'>";
				$room = $r;
			}

			echo "<a class='no_ref' href='?c=shkaf&a=detail&id=".$line["id"]."'><div class='card m05 shkf_cd'>
					<img class='card_pic' src='".$this->noPic($line["pic"])."'>
					<div class='card_desc'>
						<div class='card_head'>".$line["name"]."</div>
						<div class='card_body'><b>Описание: </b>".((!$line['descr'])?'нет':$line['descr'])." </div>
					</div>
				  </div></a>";
		}
		if ($room != '000') echo "</div>";
	} else echo "<div class='info'>Еще нет ни одного шкафа!</div>";
	?>
	
</div>