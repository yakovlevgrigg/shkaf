<div class='boxes wrp m15'>
	<div class='bcard_desc'>
		<div class='head m05'><?=$thing['name']?></div>
		<div class='m05'><b>Шкаф:</b> <?=$thing['shkaf_name']?></div>
		<div class='m05'><b>Полка:</b> <?=$thing['place_name']?></div>
		<div class='m05'><b>Владелец:</b> <?=$thing['people_name']?></div>
		<div class='m05'><b>Описание:</b> <?=$thing['descr']?></div>
	</div>
	<img class='bcard_pic' src="<?=$this->noPic($thing['pic'])?>">
	<div class='m05'>
		<a class='no_ref' href="#" onClick='document.getElementById("thing_upd_wnd").style.display = "flex"'><div class='btn m05'>Изменить эту вещь</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("thing_del_wnd").style.display = "flex"'><div class='btn m05'>Удалить эту вещь</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("change_shkaf_wnd").style.display = "flex"'><div class='btn m05'>Переложить эту вешь в другой шкаф</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("chg_owner_wnd").style.display = "flex"'><div class='btn m05'>Поменять владельца этой веши</div></a>
		<a class='no_ref' href="?c=shkaf&a=detail&id=<?=$thing['shkaf_id']?>" ><div class='btn m05'>Перейти в этот шкаф</div></a>
	</div>
</div>

<form class='wnd rows sd_cnt' enctype='multipart/form-data' method='post' id='thing_upd_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Изменить эту вещь</div>
		<div>
			<div class='p05'>
				<span class='lbl'>Название</span> <br> 
				<input class='fld full' type='text' name='name' value='<?=$thing['name']?>'>
			</div>
			<div class='p05'>
				<span class='lbl'>Описание</span> <br>
				<textarea class='fld full' name='descr'><?=$thing['descr']?></textarea>
			</div>
			<div class='p05'>
				<span class='lbl'>Добавить фото</span> <br> 
				<input class='fld full' type="file" name='pic'>
			</div>
			<input type="hidden" name='c' value='thing'>
			<input type="hidden" name='a' value='update'>
			<input type="hidden" name='id' value='<?=$thing['id']?>'>
		</div>	
		<div class='wait p05' id='wait'>
			...грузим в БД...
		</div>
		
		<div class='p05'>		
			<button class='btn  m05_r' onClick='document.getElementById("wait").style.display = "block"' type='submit'>Да</button>
			<button class='btn' onClick='document.getElementById("thing_upd_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>
</form>

<form class='wnd rows sd_cnt' method='post' id='thing_del_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Удалить эту вещь?</div>
		<input type="hidden" name='c' value='thing'>
		<input type="hidden" name='a' value='delete'>
		<input type="hidden" name='id' value='<?=$thing['id']?>'>
		<div class='p05'>
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' onClick='document.getElementById("thing_del_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>
</form>

<form class='wnd rows sd_cnt' method='post' id='change_shkaf_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Переложить в другой<br> шкаф</div>
		<div>
			<?php 
				$shkaf = -1;
				$first = true;
				foreach ($places as $line) {

					if ($shkaf != (int) $line['shkaf_id']) {
						if ($shkaf != -1) echo "</div>";
						echo "<div class='lbl m05_t'>".$line['shkaf_name']."</div> 
								<div>";
						$shkaf = (int) $line['shkaf_id'];
					}
					if ($first) {
						$first = false;
						echo "<div> <label><input type='radio' name='place_id' value='".$line["id"]."' checked> ".$line["name"]."</label> </div>";
					}else  echo "<div> <label><input type='radio' name='place_id' value='".$line["id"]."'> ".$line["name"]."</label> </div>";
				}
				if ($shkaf != -1) echo "</div>";
			?>
			<input type="hidden" name='c' value='thing'>
			<input type="hidden" name='a' value='move'>
			<input type="hidden" name='id' value='<?=$thing['id']?>'>
		</div>		
		<div class='p15'>		
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' onClick='document.getElementById("change_shkaf_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>
</form>
<form class='wnd rows sd_cnt' method='post' id='chg_owner_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Новым владельцем будет</div>
		<div class='p05'>
			<?php 
				$checked = true;
				foreach ($owners as $owner) {
					if ($checked) {
						$checked = false;
						echo "<div><label class='lbl'><input type='radio' name='owner_id' value='".$owner["id"]."' checked> ".$owner["name"]."</label> </div>";
					} else  echo "<div><label class='lbl'><input type='radio' name='owner_id' value='".$owner["id"]."'> ".$owner["name"]."</label> </div>";
				}
			?>
			<input type="hidden" name='c' value='thing'>
			<input type="hidden" name='a' value='owner'>
			<input type="hidden" name='id' value='<?=$thing['id']?>'>
		</div>		
		<div class='p05'>		
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' href="#" onClick='document.getElementById("chg_owner_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>	
</form>