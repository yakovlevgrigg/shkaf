
<div class='boxes wrp m15'>
	<div class='bcard_desc'>
		<div class='head m05'><b><?=$shkaf['name']?></b></div>
		<div class='m05'><b>Локация: </b><?=$shkaf['room_name']?></div>
		<div class='m05'><b>Описание: </b><?php echo (!$shkaf['descr'])?'отсутствует':$shkaf['descr']; ?></div>
	</div>
	<img class='bcard_pic'  src="<?=$this->noPic($shkaf['pic'])?>">
	<div class='m05'>
		<a class='no_ref' href="#" onClick='document.getElementById("new_thing_wnd").style.display = "flex"'><div class='btn m05'> Создать новую вещь</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("place_upd_wnd").style.display = "flex"'><div class='btn m05'>Создать новую полку</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("delete_place_wnd").style.display = "flex"'><div class='btn m05'>Удалить полку</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("shkaf_upd_wnd").style.display = "flex"'><div class='btn m05'>Изменить этот шкаф</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("chg_room_wnd").style.display = "flex"'><div class='btn m05'> Переставить шкаф в другую локацию</div></a>
		<a class='no_ref' href="#" onClick='document.getElementById("shkaf_del_wnd").style.display = "flex"'><div class='btn m05'>Удалить этот шкаф</div></a>
	</div>
</div>

<div class='list_head thng_hd'>В шкафу сейчас лежит</div>
<?php
	echo	"<div class='boxes wrp'>";
	if (!$things) echo "<div class='info'>Ничего не найдено.</div>";
	else foreach ($things as $line) {

		echo "<a class='no_ref' href='?c=thing&a=detail&id=".$line["id"]."'><div class='card m05 thng_cd'>
				<img class='card_pic' src='".$this->noPic($line['pic'])."'>
				<div class='card_desc'>
					<div class='card_head' >".$line["name"]."</div>
					<div class='card_body' >
						<div class='m05_b'><b>Владелец:</b> ".$line["name_people"]."</div>
						<div><b>Полка:</b> ".$line["place_name"]."</div>
					</div>
				</div>
			  </div></a>";
	}
	echo "</div>";
?>

<form class='wnd rows sd_cnt' enctype='multipart/form-data' method='post' id='shkaf_upd_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Изменить этот шкаф</div>
		<div>
			<div class='p05'>
				<span class='lbl'>Название</span> <br>
				<input class='fld full' type='text' name='name' value='<?=$shkaf['name']?>'>
			</div>
			<div class='p05'>
				<span class='lbl'>Описание</span> <br>
				<textarea class='fld full' name='descr'><?=$shkaf['descr']?></textarea>
			</div>
			<div class='p05'>
				<span class='lbl'>Добавить фото</span> <br>
				<input class='fld full' type="file" name='pic'>
			</div>
			<input type="hidden" name='c' value='shkaf'>
			<input type="hidden" name='a' value='update'>
			<input type="hidden" name='id' value='<?=$shkaf['id']?>'>
		</div>
		<div class='wait p05' id='wait'>
			...грузим в БД...
		</div>	
		<div class='p05'>		
			<button class='btn m05_r' onClick='document.getElementById("wait").style.display = "block"' type='submit'>Да</button>
			<button class='btn' onClick='document.getElementById("shkaf_upd_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>	
</form>

<form class='wnd rows sd_cnt' enctype='multipart/form-data' method='post' id='place_upd_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Создать новую полку</div>
		<div>
			<div class='p05'> 
				<span class='lbl'>Название</span><span style='color:red;'>* </span><br>
				<input class='fld full' type='text' name='name'>
			</div>
			<div class='p05'>
				<span class='lbl'>Описание</span> <br>
				<textarea class='fld full' name='descr' ></textarea>
			</div>
			<input type="hidden" name='c' value='place'>
			<input type="hidden" name='a' value='insert'>
			<input type="hidden" name='id' value='<?=$shkaf['id']?>'>
		</div>	
		<div class='p05'>		
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' href="#" onClick='document.getElementById("place_upd_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>		
</form>

<form class='wnd rows sd_cnt' method='post' id='shkaf_del_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Удалить этот шкаф?</div>
		<input type="hidden" name='c' value='shkaf'>
		<input type="hidden" name='a' value='delete'>
		<input type="hidden" name='id' value='<?=$shkaf['id']?>'>
		<div class='p05'>
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' onClick='document.getElementById("shkaf_del_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>
</form>

<form class='wnd rows sd_cnt' method='post' id='delete_place_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Удалить полку?</div>
		<div class='p05'>
			<?php 
				if (1 == count($places)) echo "Нет полок";
				else {
					$itt = 1;
					foreach ($places as $line) {
						if($line["name"] == "Где-то в шкафу") { }	
						else if ($itt == 1) {
							$itt = 2;
							echo "<div><label class='lbl'><input type='radio' name='place_id' value='".$line["id"]."' checked> ".$line["name"]."</label> </div>";
						}
						else echo "<div><label class='lbl'><input type='radio' name='place_id' value='".$line["id"]."'> ".$line["name"]."</label> </div>";
					}
				}
			?>
			<input type="hidden" name='c' value='place'>
			<input type="hidden" name='a' value='delete'>
		</div>		
		<div class='p05'>		
			<button class='btn m05_r' type='submit'>Да</button>
			<button class='btn' href="#" onClick='document.getElementById("delete_place_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>	
</form>

<form class='wnd rows sd_cnt' action='?c=thing&a=new' method='post'enctype="multipart/form-data" id="new_thing_wnd">
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Новая вещь</div>
		<div>
			<div class='p05'>
				<span class='lbl'>Название</span><span style='color:red;'>* </span><br>
				<input class='fld full' type='text' name='name'>
			</div>
			<div class='p05'>
				<span class='lbl'>Описание</span> <br>
				<textarea class='fld full' name='descr'></textarea>
			</div>
			<div class='p05'>
				<span class='lbl'>Полка</span> <br>
				<select class='fld full' name='place_id'>
					<?php
						foreach ($places as $place) {
							echo "<option value=".$place['id'].">".$place['name']."</option>";	
						}
					?>
				</select> 
			</div>
			<div class='p05'>
				<span class='lbl'>Владелец</span> <br>
				<select class='fld full' name='people_id'>
					<?php
						foreach ($people as $man) {
							echo "<option value=".$man['id'].">".$man['name']."</option>";	
						}
					?>
				</select> 
			</div>
			<div class='p05'>
				<span class='lbl'>Добавить фото</span><br>
				<input class='fld full' type="file" name='pic'>
			</div>
		</div>	
		<div class='wait p05' id='wait'>
			...грузим в БД...
		</div>
		<div class='p05'>		
			<button class='btn m05_r' onClick='document.getElementById("wait").style.display = "block"' type='submit'>Создать</button>
			<button class='btn' onClick='document.getElementById("new_thing_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>
</form>
<form class='wnd rows sd_cnt' method='post' id='chg_room_wnd'>
	<div class='rows brd sd_cnt p05 m05 sdw'>
		<div class='head p05'>Переставить шкаф в</div>
		<div class='p05'>
			<?php
				$itt = 1;	
				foreach ($rooms as $room) {
					if ($itt == 1) {
						$itt = 2;
						echo "<div><label class='lbl'><input type='radio' name='room_id' value='".$room["id"]."' checked> ".$room["name"]."</label> </div>";
					}
					else echo "<div><label class='lbl'><input type='radio' name='room_id' value='".$room["id"]."'> ".$room["name"]."</label> </div>";
				}
			?>
			<input type="hidden" name='c' value='shkaf'>
			<input type="hidden" name='a' value='room'>
			<input type="hidden" name='id' value='<?=$shkaf['id']?>'>
		</div>		
		<div class='p05'>		
			<button class='btn  m05_r' type='submit'>Да</button>
			<button class='btn' href="#" onClick='document.getElementById("chg_room_wnd").style.display = "none"' type='button'>Нет</button>
		</div>
	</div>	
</form>
	