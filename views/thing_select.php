
<form class='rows m05 sd_str' action='' method='get'>
	<div class='head m05_l'>Поиск вещей</div>
	<div class='boxes wrp brd sd_end p05'>
		<input type='hidden' name='c' value='thing'>
		<input type='hidden' name='a' value='select'>
		<div class='m05'>
			<span class='lbl'>Название </span> <br>
			<input class='fld' type='text' name='name' value=<?=$search['name']?>>
		</div>
		<div class='m05'>
			<span class='lbl'>Описание </span> <br>
			<input class='fld' type='text' name='descr'value=<?=$search['descr']?>>
		</div>
		<div class='m05'>
			<span class='lbl'>Шкаф </span> <br>
			<input class='fld' type='text' name='shkaf' value=<?=$search['shkaf']?>>
		</div>
		<div class='m05'>
			<span class='lbl'>Владелец </span> <br> 
			<select class='fld' name='people'>
				<option value="">Любой</option>
				<?php
					foreach ($people as $man) {
						echo "<option value=".$man['name'];
						if ($man['name'] == $search['people']) echo " selected ";
						echo ">".$man['name']."</option>";	
					}
				?>
			</select> 
		</div>
		<div class='m05'>
			<button class='btn m05_l' type='submit'>Искать</button>	
		</div>
	</div>			
</form>


<?php 
$shkaf = -1;
if ($res == -1) echo "<div class='info'> Задайте условия поиска.</div>";
else if (!$res) echo "<div class='info'> Ничего не найдено.</div>";
else foreach ($res as $line) {
	if ($shkaf != (int) $line['shkaf_id']) {
		if ($shkaf != -1) echo "</div>";
		echo "<div class='list_head thng_hd'> 
				Шкаф <i>".$line['shkaf_name']."</i>
			 </div>
			 <div class='boxes wrp'>";
		$shkaf = (int) $line['shkaf_id'];
	}

	echo "<a class='no_ref' href='?c=thing&a=detail&id=".$line["id"]."'>
		<div class='card m05 thng_cd'>
			<img class='card_pic' src='".$this->noPic($line["pic"])."'>
			<div class='card_desc'>
				<div class='card_head' >".$line["name"]."</div>
				<div class='card_body' >
					<div class='m05_b'><b>Владелец:</b> ".$line["name_people"]."</div>
				    <div><b>Полка:</b> ".$line["place_name"]."</div>
				</div>
			</div>
	    </div></a>";
}
if ($shkaf != -1) echo "</div>";
?>
