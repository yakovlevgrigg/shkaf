<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/main.css?a=b">
		<link rel="stylesheet" href="css/ui.css?a=b">
		<link rel="stylesheet" href="css/yashkaf.css?a=b">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
		<div class="lo_header">
			<div class="lo_logo">ЧТО В ШКАФУ?</div>
			<a class="lo_ref" href="?c=shkaf&a=select">ШКАФЫ</a>
			<a class="lo_ref" href="?c=thing&a=select">ВЕЩИ</a>
			<a class="lo_ref" href="?c=info&a=help">HELP</a>
		</div>
		<?php
			$msg = $_SESSION['msg']; 
			if ($msg) {
				unset($_SESSION['msg']);		
		?>		
				<a href='#' class="no_ref" onClick='document.getElementById("msg_box").style.display = "none"'>
		<?php
				if (substr($msg,0,4) == "ОК") echo "<div class='msg suc' id='msg_box'>";
				else echo "<div class='msg err' id='msg_box'>";
				echo $msg;
				echo "</div></a>";	
			}
		?>
		<div>
			<?=$content ?>
		</div>
	</body>
</html>