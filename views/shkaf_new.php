<div class='shkaf_new'>	
	<div class='list_shkaf_room'>Создать новый шкаф</div>

	<div class="shkaf_new_msg"><?=$msg ?></div>

	<form class='shkaf_new_form' action='?c=shkaf&a=new' method='post' enctype="multipart/form-data">
		<div class='shkaf_new_el'>Название <input class='shkaf_new_el_fld' type='text' name='name'></div>
		<div class='shkaf_new_el'>Описание <textarea class='shkaf_new_el_fld' name='descr' col=40 rows=3></textarea></div>
		<div class='shkaf_new_el'>Добавить фото <input class='shkaf_new_el_fld_pic' type="file" name='pic'></div>
		<div class='thing_new_el'>
			Комната
			<select class='thing_new_el_fld' name='room_id'>
				<?php
					foreach ($rooms as $room) {
						echo "<option value=".$room['id'].">".$room['name']."</option>";
					}
				?>
			</select> 
		</div>
		<button class='shkaf_new_btn' type='submit'>Создать</button>		
	</form>
</div>
