<div class='thing_new'>
	<div class='list_thing_shkaf'>Положить новую вещь в шкаф</div>

	<div class="thing_new_msg"><?=$msg ?></div>

	<form class='thing_new_form' action='?c=thing&a=new' method='post' enctype="multipart/form-data">
		<div class='thing_new_el'>Название <input class='thing_new_el_fld' type='text' name='name'></div>
		<div class='thing_new_el'>Описание <textarea class='thing_new_el_fld' name='descr' col=40 rows=3></textarea></div>
		<div class='thing_new_el'>
			Полка 
			<select class='thing_new_el_fld' name='place_id'>
				<?php
					foreach ($places as $place) {
						echo "<option value=".$place['id'].">".$place['name']."</option>";	
					}
				?>
			</select> 
		</div>
		<div class='thing_new_el'>
			Владелец 
			<select class='thing_new_el_fld' name='people_id'>
				<?php
					foreach ($people as $man) {
						echo "<option value=".$man['id'].">".$man['name']."</option>";	
					}
				?>
			</select> 
		</div>
		<div class='thing_new_el'>Добавить фото <input class='thing_new_el_fld_pic' type="file" name='pic'></div>
		<button class='thing_new_btn' type='submit'>Положить</button>		
	</form>
</div>
