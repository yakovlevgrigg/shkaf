<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/site.css">
		<link rel="stylesheet" href="css/thing.css">
		<link rel="stylesheet" href="css/place.css">
		<link rel="stylesheet" href="css/shkaf.css">
		<link rel="stylesheet" href="css/login.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
		<div class="lo_header">
			<div class="lo_logo">ЧТО В ШКАФУ?</div>
			<a class="lo_ref" href="?c=shkaf&a=select">ШКАФЫ</a>
			<a class="lo_ref" href="?c=thing&a=select">ВЕЩИ</a>
			<a class="lo_ref" href="?c=info&a=help">HELP</a>
		</div>
		<div class="lo_center">
			<?=$content ?>
		</div>
	</body>
</html>