<?php

class Model {

    public $db_connect;
	public $rules = []; // каждый элемент массива - тоже массив - rule[]
						// rule[0] - имя поля, rule[1] - юзерское имя поля, rule[2] - номер правила
						// правило 1 - проверка, что значение не пустое
						// правило 2 - проверка, что значение в БД уникальное
						

	public function __construct () {
		$this->db_connect = mysqli_connect('localhost','root','','shkaf');
	}
	
	public function validate($arr) {
		$msg = "";
		foreach ($this->rules as $rule) {
			foreach ($arr as $param=>$value) {
				if ($rule[0] == $param) {
					if ($rule[2] == 1) {
						if ($value == "") $msg = $msg."Поле $rule[1] не должно быть пустым. <br>";
					}
					if ($rule[2] == 2) {
						$name_unic = $this->unicheck($param, $value);						
						if ($name_unic == 1) $msg = $msg."Такое значение для поля $rule[1] уже используется. <br>";
					}
				}
			}
		}
		return $msg;
	}
	
	public function unicheck($name){
		return 0;	
 	}


}