<?php

class ShkafMod extends Model{

	public function __construct () {
		parent::__construct();
	
		$this -> rules = [
			['name', '<b>Название</b>', 1 ],
		];
		
	}
	public function unicheck($value, $name = ''){
		$query = "SELECT * FROM shkafs a WHERE a.$value='$name'";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		if ($arr) return 1;
		else return 0;			
	}

	public function selectAll() {
		$query = 'SELECT a.id, a.name, a.descr, a.pic, a.room_id, b.id as id_of_room, b.name as room_name  FROM shkafs a, rooms b WHERE a.room_id=b.id ORDER BY b.id';
		$answer = mysqli_query($this->db_connect, $query);
		return mysqli_fetch_all($answer, MYSQLI_ASSOC);
	}

	public function selectOne($id) {
		$query = "SELECT a.id, a.name, a.descr, a.pic, a.room_id, b.id as id_of_room, b.name as room_name  FROM shkafs a, rooms b WHERE a.room_id=b.id AND a.id = $id";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		return $arr[0];
	}

	public function insert($name,$descr,$pic,$room_id) {
		$query = "INSERT INTO shkafs(name,descr,pic,room_id) VALUES ('$name', '$descr', '$pic' , '$room_id')";
		$res = mysqli_query($this->db_connect, $query);
		if ($res) {
            $shkaf_id = mysqli_insert_id($this->db_connect);
			$query = "INSERT INTO places(name,descr,shkaf_id) VALUES ('Где-то в шкафу', 'Дефолтная полка', $shkaf_id)";
			mysqli_query($this->db_connect, $query);
		}
		return $res;
	}

	public function delete($id) {
		$query = "SELECT a.id  FROM things a, places b WHERE a.place_id=b.id AND b.shkaf_id=$id";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		if ($arr) return "Нельзя удалять шкаф с вещами внутри!";	
		
		$query = "SELECT pic  FROM shkafs WHERE id=$id";
		$answer = mysqli_query($this->db_connect, $query);
		$pic =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		$query = "DELETE FROM shkafs WHERE id = $id";
		$res = mysqli_query($this->db_connect, $query);
		if ($res) {
			if ($pic[0]["pic"]) unlink($pic[0]["pic"]);
			$query = "DELETE FROM places WHERE shkaf_id = $id";
			mysqli_query($this->db_connect, $query);
			return "";
		}
		return "Не получается удалить шкаф. Ошибка БД.";
	}

	public function update($id,	$name,$descr,$pic) {
		if ($pic == "") $query = "UPDATE shkafs SET name = '$name', descr = '$descr'  WHERE id = $id ";
		else {
			$q = "SELECT pic  FROM shkafs WHERE id=$id";
			$answer = mysqli_query($this->db_connect, $q);
			$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
			if ($arr[0]["pic"]) unlink($arr[0]["pic"]);
			$query = "UPDATE shkafs SET name = '$name', descr = '$descr', pic = '$pic'  WHERE id = $id ";
		}
		return  mysqli_query($this->db_connect, $query);
	}
	
	public function roomChange($id,	$room_id) {
		$query = "UPDATE shkafs SET room_id = '$room_id' WHERE id = $id";
		return  mysqli_query($this->db_connect, $query);
	}
}