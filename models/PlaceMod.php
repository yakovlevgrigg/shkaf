<?php

class PlaceMod extends Model{

	public function __construct () {
		parent::__construct();
	
		$this -> rules = [
			['name', '<b>Название</b>', 1 ],
		];
		
	}

	public function selectAllInShkaf($shkaf_id) {
		$query = "SELECT id, name FROM places WHERE shkaf_id=$shkaf_id ";
		$answer = mysqli_query($this->db_connect, $query);
		return mysqli_fetch_all($answer, MYSQLI_ASSOC);
	}

	public function selectAll() {
		$query = "SELECT a.id, a.name, b.id as shkaf_id, b.name as shkaf_name FROM places a, shkafs b WHERE a.shkaf_id = b.id ORDER BY shkaf_id";
		$answer = mysqli_query($this->db_connect, $query);
		return mysqli_fetch_all($answer, MYSQLI_ASSOC);
	}
	
	public function insert($name,$descr,$id) {
		$query = "INSERT INTO places(shkaf_id,name,descr) VALUES ('$id','$name', '$descr')";
		return mysqli_query($this->db_connect, $query);
	}
	public function delete($id) {
		$query = "SELECT a.id  FROM things a WHERE a.place_id=$id";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		if ($arr) return "Нельзя удалить полку с вещами внутри!";	
		
		$query = "DELETE FROM places WHERE id = $id";
		$res = mysqli_query($this->db_connect, $query);
		if (!$res) return "Невозможно удалить полку. Ошибка БД!";
		else return "";
	}	
}
?>