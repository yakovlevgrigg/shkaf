<?php

class ThingMod extends Model{

	public function __construct () {
		parent::__construct();
	
		$this -> rules = [
			['name', '<b>Название</b>', 1 ],
		];
		
	}

	public function unicheck($value, $name = ''){
		$query = "SELECT * FROM things a WHERE a.$value='$name'";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		if($arr) return 1;
		else return 0;	
			
	}

	public function selectSearch($name, $descr, $shkaf, $people) {
		$name = '%'.$name.'%';
		$descr = '%'.$descr.'%';
		$shkaf = '%'.$shkaf.'%';
		$people = '%'.$people.'%';
		$query = "SELECT a.id, a.name, a.descr, a.pic, b.id as place_id, b.name as place_name, c.id as shkaf_id, c.name as shkaf_name, d.name as name_people  FROM things a, places b, shkafs c, people d WHERE a.place_id=b.id AND b.shkaf_id=c.id AND a.name like '$name' AND a.descr like '$descr' AND c.name like '$shkaf' AND d.name like '$people' AND d.id=a.people_id ORDER BY c.id";
		$answer = mysqli_query($this->db_connect, $query);
		return mysqli_fetch_all($answer, MYSQLI_ASSOC);
	}
		
	public function selectShkaf($id) {
		$query = "SELECT a.id, a.name, a.descr, a.pic, b.id as place_id, b.name as place_name, c.name as name_people FROM things a, places b, people c  WHERE a.place_id=b.id AND b.shkaf_id=$id AND c.id=a.people_id ";
		$answer = mysqli_query($this->db_connect, $query);
		return mysqli_fetch_all($answer, MYSQLI_ASSOC);
	}
	
	public function selectOne($id) {
		$query = "SELECT a.id, a.name, a.descr, a.place_id, a.pic, b.id as place_id, b.name as place_name, c.id as shkaf_id, c.name as shkaf_name, d.name as people_name  FROM things a, places b, shkafs c, people d  WHERE a.place_id=b.id AND b.shkaf_id=c.id AND a.id = $id and a.people_id=d.id";
		$answer = mysqli_query($this->db_connect, $query);
		$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		return $arr[0];
	}

	public function insert($name,$descr,$place_id,$pic,$people_id) {
		$query = "INSERT INTO things(name,descr,place_id,pic,people_id) VALUES ('$name', '$descr', $place_id, '$pic', '$people_id')";
		return mysqli_query($this->db_connect, $query);
	}

	public function update($id,	$name,$descr,$pic) {
		if ($pic == "") $query = "UPDATE things SET name = '$name', descr = '$descr' WHERE id = $id ";
		else {
			$q = "SELECT pic  FROM things WHERE id=$id";
			$answer = mysqli_query($this->db_connect, $q);
			$arr =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
			if ($arr[0]["pic"]) unlink($arr[0]["pic"]);
			$query = "UPDATE things SET name = '$name', descr = '$descr', pic = '$pic' WHERE id = $id ";
		}	
		return  mysqli_query($this->db_connect, $query);
	}
	
	public function move($id,$place_id) {
		$query = "UPDATE things SET place_id = '$place_id' WHERE id = $id ";
		return  mysqli_query($this->db_connect, $query);
	}

	public function changeOwner($id,$owner_id) {
		$query = "UPDATE things SET people_id = '$owner_id' WHERE id = $id ";
		return  mysqli_query($this->db_connect, $query);
	}

	public function delete($id) {	
		$query = "SELECT pic  FROM things WHERE id=$id";
		$answer = mysqli_query($this->db_connect, $query);
		$pic =  mysqli_fetch_all($answer, MYSQLI_ASSOC);
		$query = "DELETE FROM things WHERE id = $id";
		$res = mysqli_query($this->db_connect, $query);
		if ($res) {
			if ($pic[0]["pic"]) unlink($pic[0]["pic"]);
			return "";
		} else return "Невозможно удалить вешь. Ошибка БД!";
	}
	
	
}